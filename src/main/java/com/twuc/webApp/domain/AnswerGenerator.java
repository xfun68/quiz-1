package com.twuc.webApp.domain;

import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

@Component
public class AnswerGenerator {

    public String generateAnswer() {
        List<String> allDigits = Arrays.asList("0123456789".split(""));
        Collections.shuffle(allDigits);
        return String.join("", allDigits.subList(0, 4));
    }
}