package com.twuc.webApp.domain;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class Game {

    private static Long globalId = 1L;
    private Long id;
    private String answer;

    public Game(AnswerGenerator answerGenerator) {
        this.id = generateId();
        answer = answerGenerator.generateAnswer();
    }

    public Long getId() {
        return this.id;
    }

    public String getAnswer() {
        return answer;
    }

    private Long generateId() {
        return globalId++;
    }

    public GuessResult tryGuess(Guess guess) {
        List<String> symbols = new ArrayList<>();

        IntStream.range(0, guess.getAnswer().size()).forEach(i -> {
            if (guess.getAnswerAt(i) == this.answer.charAt(i)) {
                symbols.add("A");
            } else if (this.answer.contains(String.valueOf(guess.getAnswerAt(i)))) {
                symbols.add("B");
            }
        });

        return new GuessResult(
                symbols.stream().filter(symbol -> symbol.equals("A")).count(),
                symbols.stream().filter(symbol -> symbol.equals("B")).count()
        );
    }
}
