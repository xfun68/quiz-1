package com.twuc.webApp.domain;

import java.util.Arrays;
import java.util.List;

public class Guess {
    private String answer;

    public Guess() {
    }

    public Guess(String answer) {
        this.answer = answer;
    }

    public List<String> getAnswer() {
        return Arrays.asList(answer.split(""));
    }

    public char getAnswerAt(int i) {
        return this.answer.charAt(i);
    }
}
