package com.twuc.webApp.domain;

public class GuessResult {
    private String hint;
    private Boolean correct;

    public GuessResult(long countOfA, long countOfB) {
        hint = String.format("%dA%dB", countOfA, countOfB);
        correct = countOfA == 4 && countOfB == 0;
    }

    public String getHint() {
        return hint;
    }

    public Boolean getCorrect() {
        return correct;
    }
}
