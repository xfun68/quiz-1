package com.twuc.webApp.web;

import com.twuc.webApp.domain.AnswerGenerator;
import com.twuc.webApp.domain.Game;
import com.twuc.webApp.domain.Guess;
import com.twuc.webApp.domain.GuessResult;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.net.URI;
import java.util.HashMap;
import java.util.Map;

@RestController
public class GameController {
    private Map<Long, Game> games = new HashMap<>();
    private AnswerGenerator answerGenerator;

    public GameController(AnswerGenerator answerGenerator) {
        this.answerGenerator = answerGenerator;
    }

    @PostMapping("/api/games")
    public ResponseEntity create() {
        Game game = new Game(answerGenerator);
        games.put(game.getId(), game);
        return ResponseEntity
                .created(URI.create("/api/games/" + game.getId()))
                .build();
    }

    @GetMapping("/api/games/{gameId}")
    public ResponseEntity<Game> get(@PathVariable Long gameId) {
        return ResponseEntity.ok().body(findGameById(gameId));
    }

    @RequestMapping(path = "/api/games/{gameId}", method = RequestMethod.PATCH)
    public ResponseEntity<GuessResult> guess(@PathVariable Long gameId, @RequestBody Guess guess) {
        Game game = findGameById(gameId);
        GuessResult guessResult = game.tryGuess(guess);
        return ResponseEntity.ok()
                .body(guessResult);
    }

    private Game findGameById(@PathVariable Long gameId) {
        if (!games.containsKey(gameId)) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
        return games.get(gameId);
    }
}
