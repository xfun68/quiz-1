package com.twuc.webApp;

import com.twuc.webApp.domain.AnswerGenerator;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertNotEquals;

class AnswerGeneratorTest {

    private AnswerGenerator answerGenerator;

    @BeforeEach
    void setUp() {
        answerGenerator = new AnswerGenerator();
    }

    @Test
    void test_answers_should_be_different_btw_different_games() {
        assertNotEquals(answerGenerator.generateAnswer(), answerGenerator.generateAnswer());
    }
}
