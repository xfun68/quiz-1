package com.twuc.webApp;

import org.hamcrest.Matchers;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import java.util.Objects;

import static org.hamcrest.Matchers.greaterThan;
import static org.hamcrest.Matchers.isEmptyString;
import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@SpringBootTest
@AutoConfigureMockMvc
class GameControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Test
    void should_create_a_new_game() throws Exception {
        MvcResult firstResult = mockMvc.perform(post("/api/games")).andReturn();
        MvcResult secondResult = mockMvc.perform(post("/api/games")).andReturn();

        assertEquals(HttpStatus.CREATED.value(), firstResult.getResponse().getStatus());
        assertEquals(HttpStatus.CREATED.value(), secondResult.getResponse().getStatus());

        String firstLocation = firstResult.getResponse().getHeader("Location");
        assertNotNull(firstLocation);
        assertTrue(firstLocation.matches("/api/games/\\d+"));

        String secondLocation = secondResult.getResponse().getHeader("Location");
        assertNotNull(secondLocation);
        assertTrue(secondLocation.matches("/api/games/\\d+"));

        assertNotEquals(extractGameId(firstLocation), extractGameId(secondLocation));
    }

    @Test
    void should_return_game_status() throws Exception {
        MvcResult result = mockMvc.perform(post("/api/games")).andReturn();
        String gameId = extractGameId(Objects.requireNonNull(result.getResponse().getHeader("Location")));

        mockMvc.perform(get("/api/games/" + gameId))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith("application/json"))
                .andExpect(jsonPath("$.id", greaterThan(0)))
                .andExpect(jsonPath("$.answer", Matchers.not(isEmptyString())));
    }

    @Test
    void should_return_404_when_game_does_not_exist() throws Exception {
        mockMvc.perform(get("/api/games/" + Integer.MAX_VALUE))
                .andExpect(status().isNotFound());
    }

    @Test
    void should_return_400_when_game_id_is_invalid() throws Exception {
        mockMvc.perform(get("/api/games/foobar"))
                .andExpect(status().isBadRequest());
    }

    @Test
    void should_return_result_when_guess_the_number() throws Exception {
        MvcResult result = mockMvc.perform(post("/api/games")).andReturn();
        String gameId = extractGameId(Objects.requireNonNull(result.getResponse().getHeader("Location")));

        mockMvc.perform(patch("/api/games/" + gameId)
                .content("{ \"answer\": \"1986\" }")
                .contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith("application/json"))
                .andExpect(jsonPath("$.hint", Matchers.anything()))
                .andExpect(jsonPath("$.correct", Matchers.notNullValue()));
    }

    @Test
    void should_return_404_when_guess_with_wrong_game_id() throws Exception {
        mockMvc.perform(patch("/api/games/" + Integer.MAX_VALUE)
                .content("{ \"answer\": \"1986\" }")
                .contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(status().isNotFound());
    }

    private String extractGameId(String location) {
        return location.split("/")[3];
    }
}
