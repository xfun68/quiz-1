package com.twuc.webApp;

import com.twuc.webApp.domain.AnswerGenerator;
import com.twuc.webApp.domain.Game;
import com.twuc.webApp.domain.Guess;
import com.twuc.webApp.domain.GuessResult;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class GameTest {

    private AnswerGenerator answerGenerator;
    private Game game;

    @BeforeEach
    void setUp() {
        answerGenerator = new MockAnswerGenerator("2019");
        game = new Game(answerGenerator);
    }

    @Test
    void should_return_mocked_answer() {
        assertEquals(game.getAnswer(), "2019");
    }

    @Test
    void should_return_0A0B_when_all_digits_mismatched() {
        GuessResult guessResult = game.tryGuess(new Guess("5678"));
        assertEquals("0A0B", guessResult.getHint());
    }

    @Test
    void should_return_0A2B_when_two_digits_matched_value_but_mismatched_position() {
        GuessResult guessResult = game.tryGuess(new Guess("1986"));
        assertEquals("0A2B", guessResult.getHint());
    }

    @Test
    void should_return_2A1B_when_guess_is_2046_given_answer_is_2019() {
        GuessResult guessResult = game.tryGuess(new Guess("2046"));
        assertEquals("2A0B", guessResult.getHint());
    }

    @Test
    void should_return_2A1B_when_guess_is_2081_given_answer_is_2019() {
        GuessResult guessResult = game.tryGuess(new Guess("2081"));
        assertEquals("2A1B", guessResult.getHint());
    }

    @Test
    void should_return_4A0B_when_guess_is_2019_given_answer_is_2019() {
        GuessResult guessResult = game.tryGuess(new Guess("2019"));
        assertEquals("4A0B", guessResult.getHint());
    }

    private static class MockAnswerGenerator extends AnswerGenerator {

        private String mockAnswer;

        private MockAnswerGenerator(String mockAnswer) {
            this.mockAnswer = mockAnswer;
        }
        @Override
        public String generateAnswer() {
            return this.mockAnswer;
        }

    }
}
