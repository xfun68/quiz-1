package com.twuc.webApp;

import com.twuc.webApp.domain.GuessResult;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class GuessResultTest {

    @Test
    void should_return_hint_according_to_symbols_count() {
        GuessResult guessResult = new GuessResult(1, 2);
        assertEquals("1A2B", guessResult.getHint());
    }

    @Test
    void should_return_correct_is_false_when_guess_does_not_match_all_numbers() {
        GuessResult guessResult = new GuessResult(1, 2);
        assertEquals(false, guessResult.getCorrect());
    }

    @Test
    void should_return_correct_is_true_when_guess_matches_all_numbers() {
        GuessResult guessResult = new GuessResult(4, 0);
        assertEquals(true, guessResult.getCorrect());
    }
}